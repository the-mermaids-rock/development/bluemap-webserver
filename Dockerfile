FROM node:20.16-alpine as vite

WORKDIR /app

COPY ./app /app
RUN npm install && \
    npm run build

FROM dunglas/frankenphp

# Disable HTTPS (handled by Traefik)
ENV SERVER_NAME=:80

# Install additional needed extensions
RUN install-php-extensions \
    pdo_mysql \
    opcache

# Add configuration
COPY ./Caddyfile /etc/caddy/Caddyfile
COPY ./php-opcache.ini $PHP_INI_DIR/conf.d/opcache.ini
COPY ./assets/settings.json /app/settings.json

# Copy app from build stage
COPY --from=vite /app/dist /app

# Add images
COPY ./assets/towny_town_icon.png /app/data/images/towny_town_icon.png
COPY ./assets/towny_outpost_icon.png /app/data/images/towny_outpost_icon.png
