# Bluemap-Webserver

Bluemap setup using FrankenPHP and Docker.


## Requirements

TODO

## Preparing Bluemap

TODO

## Building the image

TODO

## FAQ

### Why this solution over the internal webserver?  
To keep the map available while the server is shutdown.  
Additionally, it saves us resources that can be put into the game rather than the map.  
And finally, if there were to be a major bug in the Bluemap webserver for some reason, stability of the server wouldn't be affected as we do not use it.

### Why FrankenPHP over the "officially supported" NGINX+PHP-FPM?
Due to the way PHP-FPM works, when a request is made PHP makes it so that each request has its own "universe".  
Doing this can add a significant overhead and increase response time.  
Additionally, when there are a lot of requests incoming at the same time, more processes need to be spun up to handle the load.  
As a single client can send 10requests/s pretty easily, this can add up real quick.

FrankenPHP keeps things in memory to decrease the response time while also incurring a lower overhead.
__This feature is yet to be implemented here.__

In the end, this should all result into a faster, yet less resource-intensive application.
